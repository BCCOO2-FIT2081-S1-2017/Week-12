package com.example.darkst0rm.week12;

/*
 * Created by DarkSt0rm on 2017/05/24.
 * FIT3031 Week 12 Code
 */

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public class MainActivity extends AppCompatActivity {

    // A few constants a variables to be used
    final static String DATA_FILE_NAME = "data_file.txt";
    final static String FILE_CONTENT = "Hello, world!";
    public TextView infoBox;

    // A simple utility function to easily append Strings into the infoBox
    public void println(String s) {
        infoBox.append(s);
        infoBox.append("\n");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        infoBox = (TextView) findViewById(R.id.infobox);

        println("Welcome to the Storage Testing Utility");
        println("This utility will test reading and writing capabilities of the android system");
        println("=========== START ===========");
        println("Internal (Local) Storage:");

        // Try to open a new file for writing, catch an exception if any
        try (FileOutputStream os = openFileOutput(DATA_FILE_NAME, Context.MODE_PRIVATE)) {
            os.write(FILE_CONTENT.getBytes());
            println("\tWrote the string " + FILE_CONTENT + " to file " + DATA_FILE_NAME);
        } catch (IOException e) {
            println("\tFailed to write " + DATA_FILE_NAME + " due to " + e);
        }

        File where = getFilesDir(); // Absolute path to directory for our app's internal storage
        println("\tThe private dir is " + where.getAbsolutePath());

        // Try to read the string which was written to the above file, catch an exception if any
        try (BufferedReader is = new BufferedReader(new InputStreamReader(openFileInput(DATA_FILE_NAME)))) {
            // Just read the first line, since it is only a single line...
            String line = is.readLine();
            println("\tRead the string: " + line);
        } catch (IOException e) {
            println("\tFailed to read back " + DATA_FILE_NAME + " due to " + e);
        }

        // Create a new cache file in the local directory
        File cacFile = new File(getCacheDir(), "appCache.dat");
        println("\tA cache file is at " + cacFile.getAbsolutePath());

        File tmpDir = getDir("tmp2", 0);    // Creates and/or returns Dir within FilesDir
        println("\tA sub-folder is at " + tmpDir.getAbsolutePath());
        try {
            final boolean newFile = new File(tmpDir, "X").createNewFile();
            println("\tCreated file X");
        } catch (IOException e) {
            println("\tFailed to create file X due to " + e);
        }
        String[] files = fileList();
        for (String f : files) {
            println("\tFound " + f);
        }

        // Print a new empty line to divide
        println("");

        println("External Storage:");
        // Get the system state for external storage and report that state
        String state = Environment.getExternalStorageState();
        println("\tExternal storage state = " + state);
        switch (state) {
            case Environment.MEDIA_MOUNTED_READ_ONLY:
                println("\tExternal storage is read-only!!");
                break;
            case Environment.MEDIA_MOUNTED:
                println("\tExternal storage is usable");
                break;
            default:
                println("\tExternal storage NOT USABLE");
                break;
        }

        println("=========== END ===========");
    }

}